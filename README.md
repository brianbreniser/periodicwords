### Periodic words

I wanted to know what words I could make with the periodic table of element symbols

This is NOT at all optimized, but it does produce correct output, even if a word has 2 solution

examples:
    he -> He
    no -> NO, No

