#!/usr/bin/env python3

import unittest
import random

def get_periodic_children(ans_word_tuple: tuple) -> list:
    periodicTable = [
                     'H'  , 'He' , 'Li' , 'Be' , 'B'  , 'C'  , 'N'  , 'O'  , 'F'  , 'Ne' ,
                     'Na' , 'Mg' , 'Al' , 'Si' , 'P'  , 'S'  , 'Cl' , 'Ar' , 'K'  , 'C'  ,
                     'Sc' , 'Ti' , 'V'  , 'Cr' , 'Mn' , 'Fe' , 'Co' , 'Ni' , 'Cu' , 'Zn' ,
                     'Ga' , 'Ge' , 'As' , 'Se' , 'Br' , 'Kr' , 'Rb' , 'Sr' , 'Y'  , 'Zr' ,
                     'Nb' , 'Mo' , 'Tc' , 'Ru' , 'Rh' , 'Pd' , 'Ag' , 'Cd' , 'In' , 'Sn' ,
                     'Sb' , 'Te' , 'I'  , 'Xe' , 'Cs' , 'Ba' , 'La' , 'Ce' , 'Pr' , 'Nd' ,
                     'Pm' , 'Sm' , 'Eu' , 'Gd' , 'Tb' , 'Dy' , 'Ho' , 'Er' , 'Tm' , 'Yb' ,
                     'Lu' , 'Hf' , 'Ta' , 'W'  , 'Re' , 'Os' , 'Ir' , 'Pt' , 'Au' , 'Hg' ,
                     'Ti' , 'Pb' , 'Bi' , 'Po' , 'At' , 'Rn' , 'Fr' , 'Ra' , 'Ac' , 'Th' ,
                     'Pa' , 'U'  , 'Np' , 'Pu' , 'Am' , 'Cm' , 'Bk' , 'Cf' , 'Es' , 'Fm' ,
                     'Md' , 'No' , 'Lr' , 'Rf' , 'Db' , 'Sd' , 'Bh' , 'Hs' , 'Mt' , 'Ds' ,
                     'Rg' , 'Cn' , 'Uut', 'Fl' , 'Uup', 'Lv' , 'Uus', 'Uuo'
                    ]
    answers = []

    incoming_answer = ans_word_tuple[0]
    incoming_word = ans_word_tuple[1]

    # if what we have incoming is already a finished answer, just return
    if incoming_word == "":
        answers.append((incoming_answer, incoming_word))
        return answers

    # find all potential next step answers
    for i in periodicTable:
        if incoming_word.startswith(i.lower()):
            answers.append( (incoming_answer + i, incoming_word.replace(i.lower(), '', 1)) )

    return answers

def get_periodic_words(word: str) -> list:
    def is_done(l: list) -> bool:
        for i in l:
            if i[1] != "":
                return False
        return True

    answers = []

    answers.append( ("", word) )

    while not is_done(answers):
        # create a 1 level list of list (lol)
        lol_answers = [ get_periodic_children(i) for i in answers ]

        # flattens a 1 level list of list
        answers = [item for sublist in lol_answers for item in sublist]

    # get that first tuple from each list
    return [x[0] for x in answers]




def main():

    # Grab dictionary from disk
    words_file = open('/usr/share/dict/words')
    disk_dictionary = words_file.readlines()
    words_file.close()

    # Build our dictionary
    dictionary = map(lambda x: x[:-1], disk_dictionary)
    # dictionary = random.sample(list(dictionary), 100)

    print("Starting to find solutions to words...")
    print()

    answers = [(i, get_periodic_words(i)) for i in dictionary]
    answers = [ i for i in answers if len(i[1]) > 0 ]
    answers = [ (i[0], set(i[1])) for i in answers ]

    for i in answers:
        print(i[0] + ": " + str(i[1]))

if __name__ == "__main__":
    main()




class Test_get_periodic_children(unittest.TestCase):
    def test_h(self):
        self.assertEqual([('H', "")], get_periodic_children(("", "h")))

    def test_he(self):
        self.assertEqual([('H', "e"), ("He", "")], get_periodic_children(("", "he")))

    def test_uup(self):
        self.assertEqual([('U', "up"), ("Uup", "")], get_periodic_children(("", "uup")))

    def test_uup_round_2_0(self):
        self.assertEqual([('UU', "p")], get_periodic_children(("U", "up")))

    def test_uup_round_2_1(self):
        self.assertEqual([('Uup', "")], get_periodic_children(("Uup", "")))


class Test_get_periodic_words(unittest.TestCase):
    def test_h(self):
        self.assertEqual(['H'], get_periodic_words('h'))

    def test_he(self):
        self.assertEqual(['He'], get_periodic_words('he'))

    def test_woods(self):
        self.assertEqual(['He'], get_periodic_words('he'))

    def test_no(self):
        self.assertEqual(['NO', 'No'], get_periodic_words('no'))


